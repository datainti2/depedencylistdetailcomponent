import { Component, OnInit } from '@angular/core';
import {ColorPickerService} from 'ngx-color-picker';
declare var jQuery:any;
declare var carousel:any;
@Component({
  selector: 'app-base-widget',
  template: '<div class="col-md-12"><table  class="table table-hover table-outline mb-0 hidden-sm-down" style="margin-top: 22px;"><thead  class="thead-default"><tr ><th >Name</th><th  class="text-xs-center">Color Of Topic</th></tr></thead><tbody ><tr *ngFor = "let item of data"><td ><div  class="account-container"><div  class="col-md-12"><div  class="col-md-9"><div >Najib Razak</div><div  class="small text-muted"><span >najib;&amp;razak</span></div></div><div  class="col-md-2"><div  class="btn btn-sm btn-outline-primary delete-account"><i  class="glyphicon glyphicon-trash"></i></div></div></div></div></td><td  class="text-xs-center" style=" padding-top: 1.4rem;"><!--<input  style="background: rgb(55, 96, 146);">--><input [colorPicker]="item.color" (colorPickerChange)="item.color=$event" [style.background]="item.color" [value]="item.color"/></td></tr>  </tbody> </table></div>',
  styleUrls: ['./base-widget.component.css']
})
export class BaseWidgetDepedencyListDetailComponent implements OnInit {
public data: Array<any> = [
 {
  "color": "#e23f3f",
  "name": "rina",
  "count": "22220",
 
 },
 {
  "color": "#d3d3d3",
  "name": "abraham",
  "count": "13242546",
 
 }
 ];
  public color: string = "#127bdc";
    constructor(public cpService: ColorPickerService) {
    }

  ngOnInit() {
    console.log(this.data);

  }

}