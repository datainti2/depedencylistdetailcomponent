import { NgModule, ModuleWithProviders } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MaterialModule } from '@angular/material';
import { BaseWidgetDepedencyListDetailComponent } from './src/base-widget/base-widget.component';
import {ColorPickerModule} from 'ngx-color-picker';

export * from './src/base-widget/base-widget.component';

@NgModule({
  imports: [
    CommonModule,
	MaterialModule,
	ColorPickerModule
  ],
  declarations: [
	BaseWidgetDepedencyListDetailComponent
  ],
  exports: [
  BaseWidgetDepedencyListDetailComponent
  ]
})
export class DepedencyListDetailModule {
  static forRoot(): ModuleWithProviders {
    return {
      ngModule: DepedencyListDetailModule,
      providers: []
    };
  }
}
